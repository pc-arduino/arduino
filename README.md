# README
## Description
This is the Virtuose library repo, it is the basis of all the work on the virtuose platform.


## Installation

- Install PlatformIO core (https://docs.platformio.org/en/latest//core/installation.html#super-quick-mac-linux)
- Install visual studio code and the PIO extention on visual studio code (https://platformio.org/platformio-ide)
- Clone repo
- Run "pio run" in command line or cmd for windows

## Usage

To compile run "pio run"
To upload to teensy board use "pio run -t upload"


## Troubleshooting

If #includes cannot find the source, delete .vscode and restard visual studio code (https://community.platformio.org/t/cannot-open-source-file-arduino-h-when-i-have-native-environment/12145)